app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.
    when('/', {templateUrl: 'partials/dashboard.html', controller: 'dashboardController', pageName: 'Dashboard'}).
    when('/search', {templateUrl: 'partials/search.html', pageName: 'Search'}).
    when('/login', {templateUrl: 'partials/login.html', controller: 'loginController', pageName: 'Login'}).
    when('/bars', {templateUrl: 'partials/bars.html', pageName: 'Bars'}).
    when('/bars/:id', {templateUrl: 'partials/bar-singular.html', controller: 'barController', pageName: 'Bar'}).
    when('/settings', {templateUrl: 'partials/settings.html', pageName: 'Settings'}).
    otherwise({redirectTo: '/'});
}]);